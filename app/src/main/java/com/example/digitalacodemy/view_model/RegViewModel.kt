package com.example.digitalacodemy.view_model

import android.util.Log
import androidx.databinding.ObservableField
import com.example.digitalacodemy.model.PreferenceHelper
import com.example.digitalacodemy.model.dto.AuthDTO
import com.example.digitalacodemy.other.App
import com.example.digitalacodemy.other.utils.SingleLiveEvent

class RegViewModel : BaseViewModel() {

    var role: String = ""

    val name = ObservableField<String>()
    val fam = ObservableField<String>()
    val result = SingleLiveEvent<Boolean>()

    init {
        App.getComponent()?.inject(this)
    }


    fun postData() {
        Log.d("myLog", "role - $role name- ${name.get()} fam - ${fam.get()}")
        if (role.isNotEmpty() && !name.get().isNullOrEmpty() && !fam.get().isNullOrEmpty() ) {

            addDisposable(model.login(AuthDTO(name.get() + fam.get(), role))
                .subscribe({ onSuccessPostData(it) }, { error-> onFiled(error) }))
        }
    }

    private fun onSuccessPostData(idUser: String) {
        if (idUser.isNotEmpty()) {
            preferenceHelper.putString(PreferenceHelper.ID_USER, idUser)
            result.value = true
        }
    }

}