package com.example.digitalacodemy.view_model


import androidx.databinding.ObservableField
import com.example.digitalacodemy.model.PreferenceHelper
import com.example.digitalacodemy.other.App
import com.example.digitalacodemy.other.utils.SingleLiveEvent
import com.example.digitalacodemy.view.mappers.CoursesMapper
import com.example.digitalacodemy.view.vo.CourseVO
import com.example.digitalacodemy.view.vo.UserVO
import javax.inject.Inject

class CoursesViewModel : BaseViewModel() {

    val coursesSingle = SingleLiveEvent<List<CourseVO>>()

    @Inject
    lateinit var coursesMapper: CoursesMapper


    var userVO = ObservableField<UserVO>()

    init {
        App.getComponent()?.inject(this)
    }


    fun loadCourse() {
        val idUser = preferenceHelper.getString(PreferenceHelper.ID_USER)
        idUser?.let {
            model.getCourses(it)
                .map(coursesMapper)
                .subscribe({onSuccess(it)}, {error -> onFiled(error)})
        }
    }

    fun loadUserData() {
        val idUser = preferenceHelper.getString(PreferenceHelper.ID_USER)
        idUser?.let { model.getUser(it)
            .map { userDto -> UserVO(userDto.progress, userDto.role, userDto.cookies.toString()) }
            .subscribe( { onSuccessGetUser(it) }, { error -> onFiled(error) }) }
    }

    private fun onSuccessGetUser(userVO: UserVO) {
        this.userVO.set(userVO)
    }

    private fun onSuccess(courseList: List<CourseVO>) {
        coursesSingle.postValue(courseList)
    }

}