package com.example.digitalacodemy.view_model

import com.example.digitalacodemy.model.PreferenceHelper
import com.example.digitalacodemy.other.App
import com.example.digitalacodemy.other.utils.SingleLiveEvent

class WelcomeScreenViewModel : BaseViewModel() {

    val result = SingleLiveEvent<Boolean>()

    init {
        App.getComponent()?.inject(this)
    }

    fun finish() {
        preferenceHelper.putBoolean(PreferenceHelper.WAS_FIRST_START, true)
        result.postValue(true)
    }
}