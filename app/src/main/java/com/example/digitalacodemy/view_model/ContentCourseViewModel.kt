package com.example.digitalacodemy.view_model

import androidx.databinding.ObservableField
import com.example.digitalacodemy.model.PreferenceHelper
import com.example.digitalacodemy.other.App
import com.example.digitalacodemy.other.utils.SingleLiveEvent
import com.example.digitalacodemy.view.adapters.IBaseItemVm
import com.example.digitalacodemy.view.vo.ContentCourseVO

class ContentCourseViewModel : BaseViewModel() {

    val name = ObservableField<String>()
    val description = ObservableField<String>()
    val role = ObservableField<String>()
    val lessons = SingleLiveEvent<List<IBaseItemVm>>()


    init {
        App.getComponent()?.inject(this)
    }


    fun loadContentCourses(idCourse: String) {
        val idUser = preferenceHelper.getString(PreferenceHelper.ID_USER)
        idUser?.let {
            model.getCourse(idCourse, it)
                .map { course -> ContentCourseVO(course.name, course.description,
                    course.lessons, course.role) }
                .subscribe()
        }?.let { addDisposable(it) }
    }


    private fun onSuccessLoadContentCourses(contentCourseVO: ContentCourseVO) {

        name.set(contentCourseVO.name)
        description.set(contentCourseVO.description)
        role.set(contentCourseVO.role)
        //lessons.postValue(contentCourseVO)


    }

}