package com.example.digitalacodemy.view_model

import androidx.lifecycle.ViewModel
import com.example.digitalacodemy.model.PreferenceHelper
import com.example.digitalacodemy.other.App
import javax.inject.Inject

class SharedViewModel : ViewModel() {

    @Inject
    lateinit var preferenceHelper: PreferenceHelper

    init {
        App.getComponent()?.inject(this)
    }


    fun wasFirstStart(): Boolean? = preferenceHelper.getBoolean(PreferenceHelper.WAS_FIRST_START)

    fun notRegistration(): Boolean = preferenceHelper
        .getString(PreferenceHelper.ID_USER).isNullOrEmpty()

}