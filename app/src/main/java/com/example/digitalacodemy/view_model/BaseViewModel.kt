package com.example.digitalacodemy.view_model

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.example.digitalacodemy.model.Model
import com.example.digitalacodemy.model.PreferenceHelper
import com.example.digitalacodemy.other.utils.SingleLiveEvent
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import javax.inject.Inject

abstract class BaseViewModel: ViewModel() {

    @Inject
    lateinit var model: Model
    @Inject
    lateinit var preferenceHelper: PreferenceHelper
    @Inject
    lateinit var compositeDisposable: CompositeDisposable

    val errorMessage: SingleLiveEvent<String> = SingleLiveEvent()

    var isLoading = ObservableField(false)

    fun addDisposable(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    fun onFiled(throwable: Throwable) {
        isLoading.set(false)

        errorMessage.postValue(throwable.message)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}