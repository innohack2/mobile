package com.example.digitalacodemy

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.digitalacodemy.view_model.SharedViewModel

class MainActivity : AppCompatActivity() {

    private var navigateController: NavController? = null
    private val sharedViewModel: SharedViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme_NoActionBar)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        navigateController = Navigation.findNavController(this, R.id.nav_host_fragment)

        when {
            sharedViewModel.wasFirstStart() != true ->
                navigateController?.navigate(R.id.welcomeScreenFragment)
            sharedViewModel.notRegistration() -> navigateController?.navigate(R.id.regFragment)
            else -> navigateController?.navigate(R.id.cursesFragment)
        }
    }
}