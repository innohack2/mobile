package com.example.digitalacodemy.other.di

import com.example.digitalacodemy.model.ModelImple
import com.example.digitalacodemy.view.fragments.CoursesFragment
import com.example.digitalacodemy.view_model.*
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, ModelModule::class, ViewModuleModule::class])
interface AppComponent {

    fun listComponent(listCoursesModule: ListCoursesModule?): ListCoursesComponent?

    fun inject(modelImple: ModelImple)
    fun inject(welcomeScreenViewModel: WelcomeScreenViewModel)
    fun inject(sharedViewModel: SharedViewModel)
    fun inject(regViewModel: RegViewModel)
    fun inject(coursesViewModel: CoursesViewModel)
    fun inject(contentCourseViewModel: ContentCourseViewModel)

}