package com.example.digitalacodemy.other.di

import com.example.digitalacodemy.view.adapters.VmListAdapter
import com.example.digitalacodemy.view.fragments.CoursesFragment
import dagger.Module
import dagger.Provides

@Module
class ListCoursesModule (private val fragment: CoursesFragment? = null) {

    @ListScope
    @Provides
    fun provideAdapter() = VmListAdapter()
}