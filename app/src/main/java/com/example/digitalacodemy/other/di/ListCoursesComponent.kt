package com.example.digitalacodemy.other.di

import com.example.digitalacodemy.view.fragments.CoursesFragment
import dagger.Subcomponent


@ListScope
@Subcomponent(modules = [ListCoursesModule::class])
interface ListCoursesComponent {
    fun inject(fragment: CoursesFragment?)
}