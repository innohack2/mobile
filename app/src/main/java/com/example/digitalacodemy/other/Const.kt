package com.example.digitalacodemy.other

interface Const {
    companion object {
        const val UI_THREAD = "UI_THREAD"
        const val IO_THREAD = "IO_THREAD"
        const val BASE_URL = "https://test.lapshin.tech" //прод
    }
}