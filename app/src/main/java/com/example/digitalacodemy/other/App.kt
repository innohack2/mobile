package com.example.digitalacodemy.other

import android.app.Application
import android.content.Context
import com.example.digitalacodemy.other.di.*
import com.example.digitalacodemy.view.fragments.CoursesFragment


open class App :Application() {

    companion object {
        private var component: AppComponent? = null
        private var listCoursesComponent: ListCoursesComponent? = null
        fun getComponent(): AppComponent? {
            return component
        }
        fun initListCoursesComponent(fragment: CoursesFragment): ListCoursesComponent? {
            listCoursesComponent = component?.listComponent(ListCoursesModule(fragment));
            return listCoursesComponent
        }
        fun getListCoursesComponent(): ListCoursesComponent? {
            return listCoursesComponent
        }
        fun destroyListCoursesComponent() {
            listCoursesComponent = null
        }
    }

    override fun onCreate() {
        super.onCreate()
        component = buildComponent()
    }

    protected open fun buildComponent(): AppComponent {
        return DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .modelModule(ModelModule())
            .build()
    }


    override fun attachBaseContext(base: Context) {
        try {
            super.attachBaseContext(base)
        } catch (ignored: RuntimeException) {
            // Multidex support doesn't play well with Robolectric yet
        }

    }
}