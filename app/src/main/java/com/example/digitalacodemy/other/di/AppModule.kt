package com.example.digitalacodemy.other.di

import android.content.Context
import com.example.digitalacodemy.model.PreferenceHelper
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(_appContext: Context) {
    var appContext : Context = _appContext

    @Provides
    @Singleton
    internal fun provideContext(): Context {
        return appContext
    }

    @Provides
    @Singleton
    internal fun preferenceHelper(): PreferenceHelper {
        return PreferenceHelper(appContext)
    }
}