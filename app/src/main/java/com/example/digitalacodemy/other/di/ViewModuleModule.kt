package com.example.digitalacodemy.other.di

import com.example.digitalacodemy.model.Model
import com.example.digitalacodemy.model.ModelImple
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

@Module
class ViewModuleModule {

    @Provides
    internal fun compositeDisposable() : CompositeDisposable { return CompositeDisposable() }
    @Provides
    internal fun provideModel(): Model {return ModelImple() }
}