package com.example.digitalacodemy.model.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class LessonDTO (
    /**
     * Идентификатор урока
     * (Required)
     *
     */
    @SerializedName("id") @Expose var id: String,
    /**
     * Название урока
     * (Required)
     *
     */
    @SerializedName("name") @Expose var description: String,
    /**
     * Статус прохождения
     * (Required)
     *
     */
    @SerializedName("passed") @Expose var passed: Boolean

    )