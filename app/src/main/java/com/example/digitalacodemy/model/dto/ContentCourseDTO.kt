package com.example.digitalacodemy.model.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ContentCourseDTO (
    /**
     * Название курса
     * (Required)
     *
     */
    @SerializedName("name") @Expose var name: String,
    /**
     * Описание курса
     * (Required)
     *
     */
    @SerializedName("description") @Expose var description: String,
    /**
     * Список уроков
     * (Required)
     *
     */
    @SerializedName("lessons") @Expose var lessons: List<LessonDTO>,
    /**
     * Описание курса
     * (Required)
     *
     */
    @SerializedName("role") @Expose var role: String

)