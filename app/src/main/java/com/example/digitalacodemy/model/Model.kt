package com.example.digitalacodemy.model

import com.example.digitalacodemy.model.dto.*
import io.reactivex.Single
import retrofit2.http.Path

interface Model {
    fun login(authDTO: AuthDTO): Single<String>
    fun getCourses(userId: String): Single<List<CourseDTO>>
    fun getCourse(id: String, userId: String): Single<ContentCourseDTO>
    fun getLesson(id: String): Single<ContentLessonDTO>
    fun getTest(id: String): Single<TestDTO>
    fun postAnswer(answerDTO: AnswerDTO): Single<String>
    fun getUser(id: String): Single<UserDTO>
}