package com.example.digitalacodemy.model

import com.example.digitalacodemy.model.api.ApiInterface
import com.example.digitalacodemy.model.dto.*
import com.example.digitalacodemy.other.App
import com.example.digitalacodemy.other.Const
import io.reactivex.Scheduler
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Named

class ModelImple : Model {

    @Inject
    lateinit var apiInterface: ApiInterface

    @Inject @Named(Const.IO_THREAD)
    lateinit var ioThread: Scheduler

    @Inject @Named(Const.UI_THREAD)
    lateinit var uiThread: Scheduler

    init {
        App.getComponent()?.inject(this)
    }

    private fun <T> Single<T>.applySchedulers(): Single<T> =
        this.subscribeOn(ioThread)
            .observeOn(uiThread)
            .unsubscribeOn(ioThread)

    override fun login(authDTO: AuthDTO): Single<String> {
        return apiInterface.login(authDTO).applySchedulers()
    }

    override fun getCourses(userId: String): Single<List<CourseDTO>> {
        return apiInterface.getCourses(userId).applySchedulers()
    }

    override fun getCourse(id: String, userId: String): Single<ContentCourseDTO> {
        return apiInterface.getCourse(id, userId).applySchedulers()
    }

    override fun getLesson(id: String): Single<ContentLessonDTO> {
        return apiInterface.getLesson(id).applySchedulers()
    }

    override fun getTest(id: String): Single<TestDTO> {
        return apiInterface.getTest(id).applySchedulers()
    }

    override fun postAnswer(answerDTO: AnswerDTO): Single<String> {
        return apiInterface.postAnswer(answerDTO).applySchedulers()
    }

    override fun getUser(id: String): Single<UserDTO> {
        return apiInterface.getUser(id).applySchedulers()
    }
}