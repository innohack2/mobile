package com.example.digitalacodemy.model.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class QuestionDTO (
    /**
     * Заголовок вопроса
     * (Required)
     *
     */
    @SerializedName("title") @Expose var title: String,
    /**
     * Варианты ответов
     * (Required)
     *
     */
    @SerializedName("answers") @Expose var answers: String

    )