package com.example.digitalacodemy.model.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class CourseDTO (
    /**
     * Идентификатор курса
     * (Required)
     *
     */
    @SerializedName("id") @Expose var id: String,
    /**
     * Название курса
     * (Required)
     *
     */
    @SerializedName("name") @Expose var name: String,
    /**
     * Тип курса курса
     * (Required)
     *
     */
    @SerializedName("type") @Expose var type: String,
    /**
     * Прогресс по курсу
     * (Required)
     *
     */
    @SerializedName("progress") @Expose var progress: String

)