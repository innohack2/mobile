package com.example.digitalacodemy.model.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class AnswerDTO (

    /**
     * Идентификатор теста
     * (Required)
     *
     */
    @SerializedName("test_id") @Expose var name: String,
    /**
     * Идентификатор пользователя
     * (Required)
     *
     */
    @SerializedName("user_id") @Expose var userId: String,
    /**
     * Идентификатор пользователя
     * (Required)
     *
     */
    @SerializedName("answers") @Expose var answers: List<String>

    )