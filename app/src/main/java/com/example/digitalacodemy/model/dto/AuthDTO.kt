package com.example.digitalacodemy.model.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class AuthDTO (
    /**
     * Имя пользователя
     * (Required)
     *
     */
    @SerializedName("name") @Expose var name: String,
    /**
     * Роль пользователя
     * (Required)
     *
     */
    @SerializedName("role") @Expose var role: String

    )