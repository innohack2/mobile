package com.example.digitalacodemy.model.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class UserDTO (
    /**
     * Прогресс пользователя
     * (Required)
     *
     */
    @SerializedName("progress") @Expose var progress: String,
    /**
     * Роль пользователя
     * (Required)
     *
     */
    @SerializedName("role") @Expose var role: String,
    /**
     * Количество печенья
     * (Required)
     *
     */
    @SerializedName("cookies") @Expose var cookies: Int

    )