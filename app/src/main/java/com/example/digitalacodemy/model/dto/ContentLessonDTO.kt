package com.example.digitalacodemy.model.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ContentLessonDTO (
    /**
     * Название урока
     * (Required)
     *
     */
    @SerializedName("name") @Expose var name: String,
    /**
     * Содержимое урока
     * (Required)
     *
     */
    @SerializedName("content") @Expose var content: String

    )