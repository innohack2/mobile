package com.example.digitalacodemy.model.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class TestDTO (
    /**
     * Идентификатор теста
     * (Required)
     *
     */
    @SerializedName("id") @Expose var name: String,
    /**
     * Описание теста
     * (Required)
     *
     */
    @SerializedName("description") @Expose var description: String,
    /**
     * Описание теста
     * (Required)
     *
     */
    @SerializedName("questions") @Expose var questions: List<QuestionDTO>
    )