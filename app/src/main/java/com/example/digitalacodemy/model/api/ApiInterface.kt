package com.example.digitalacodemy.model.api

import com.example.digitalacodemy.model.dto.*
import io.reactivex.Single
import retrofit2.http.*

interface ApiInterface {


    @POST("/login")
    fun login(@Body authDTO: AuthDTO): Single<String>

    @GET("/courses")
    fun getCourses(@Query("user_id") userId: String): Single<List<CourseDTO>>


    @GET("/courses{id}")
    fun getCourse(@Path("id") id: String,
                  @Query("user_id") userId: String): Single<ContentCourseDTO>

    @GET("/lessons{id}")
    fun getLesson(@Path("id") id: String): Single<ContentLessonDTO>

    @GET("/lesson{id}/test")
    fun getTest(@Path("id") id: String):Single<TestDTO>

    @POST("/answers")
    fun postAnswer(@Body answerDTO: AnswerDTO): Single<String>

    @GET("/users/{id}")
    fun getUser(@Path("id") id: String): Single<UserDTO>

}