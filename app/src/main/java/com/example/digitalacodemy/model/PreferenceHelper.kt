package com.example.digitalacodemy.model

import android.content.Context
import android.content.SharedPreferences

class PreferenceHelper(_context: Context) {

    companion object {
        const val ID_USER = "id_user"
        const val NAME_USER = "name_user"
        const val FAM_USER = "fam_user"
        const val WAS_FIRST_START = "was_first_start"
    }

    private var preferences: SharedPreferences = _context
        .getSharedPreferences("preferences", Context.MODE_PRIVATE)


    fun putString(key: String, value: String) {
        val editor = preferences.edit()
        editor.putString(key, value)
        editor.apply()
    }

    fun putBoolean(key: String, value: Boolean) {
        val editor = preferences.edit()
        editor.putBoolean(key, value)
        editor.apply()
    }


    fun getString(key: String): String? {
        return preferences.getString(key, "")
    }

    fun getBoolean(key: String): Boolean? {
        return preferences.getBoolean(key, false)
    }
}