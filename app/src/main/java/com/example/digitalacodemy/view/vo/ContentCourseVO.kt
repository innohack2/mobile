package com.example.digitalacodemy.view.vo

import com.example.digitalacodemy.model.dto.LessonDTO

class ContentCourseVO (var name: String, var description: String, var lessons: List<LessonDTO>,
                       var role: String)