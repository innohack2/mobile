package com.example.digitalacodemy.view.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.digitalacodemy.R
import com.example.digitalacodemy.databinding.FragmentCoursesBinding
import com.example.digitalacodemy.other.App

import com.example.digitalacodemy.view.adapters.VmListAdapter
import com.example.digitalacodemy.view.vo.separators.GeneralCoursesSeparatorVO
import com.example.digitalacodemy.view.vo.separators.PassedCoursesSeparatorVO
import com.example.digitalacodemy.view.vo.separators.UserCoursesSeparatorVO
import com.example.digitalacodemy.view_model.CoursesViewModel
import javax.inject.Inject


class CoursesFragment : Fragment() {

    lateinit var binding: FragmentCoursesBinding

    val viewModel: CoursesViewModel by viewModels()

    @Inject
    lateinit var adapter: VmListAdapter

    private var llm: LinearLayoutManager? = null

    init {
        App.initListCoursesComponent(this)?.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_courses,
            container, false)
        binding.viewModel = viewModel

        llm = LinearLayoutManager(context)

        binding.rvCourses.layoutManager = LinearLayoutManager(context)
        binding.rvCourses.adapter = adapter


        viewModel.loadUserData()
        viewModel.loadCourse()

        viewModel.coursesSingle.observe(viewLifecycleOwner, { courses ->

            adapter.add(UserCoursesSeparatorVO())
            adapter.add(courses.filter { it.type == "user" })

            adapter.add(GeneralCoursesSeparatorVO())
            adapter.add(courses.filter { it.type == "general" })

            val passedCourses = courses.filter { it.type == "passed" }

            if (passedCourses.isNotEmpty()) {
                adapter.add(PassedCoursesSeparatorVO())
                adapter.add(courses.filter { it.type == "passed" })
            }
        })


        binding.executePendingBindings()

        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        App.destroyListCoursesComponent()
    }


}