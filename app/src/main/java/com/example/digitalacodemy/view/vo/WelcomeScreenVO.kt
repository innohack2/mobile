package com.example.digitalacodemy.view.vo

import android.graphics.drawable.Drawable


class WelcomeScreenVO(val description: String, val imgScreen: Drawable?)