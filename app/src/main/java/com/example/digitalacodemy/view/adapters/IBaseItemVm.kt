package com.example.digitalacodemy.view.adapters

interface IBaseItemVm : IBaseListItem {
    val brVariableId: Int
    val id: String?
}