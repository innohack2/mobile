package com.example.digitalacodemy.view.vo.separators

import com.example.digitalacodemy.BR
import com.example.digitalacodemy.R
import com.example.digitalacodemy.view.adapters.IBaseItemVm

class UserCoursesSeparatorVO(override val id: String? = "") : IBaseItemVm {
    override val brVariableId: Int
        get() = BR.vmUserCoursesSeparatorVO

    override fun getLayoutId(): Int = R.layout.model_user_courses_separator
}