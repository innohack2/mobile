package com.example.digitalacodemy.view.vo.separators

import com.example.digitalacodemy.BR
import com.example.digitalacodemy.R
import com.example.digitalacodemy.view.adapters.IBaseItemVm

class GeneralCoursesSeparatorVO(override val id: String = "") : IBaseItemVm {
    override val brVariableId: Int
        get() = BR.vmGeneralCoursesSeparatorVO
    override fun getLayoutId(): Int = R.layout.model_general_caurses_separator
}