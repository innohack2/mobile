package com.example.digitalacodemy.view.adapters



interface IBaseListAdapter<T> {
    fun add(newItem: T)
    fun add(newItems: List<T>)
    fun add(position: Int, newItems: List<T>)
    fun addAtPosition(pos : Int, newItem : T)
    fun replaceItems(newItems: List<T>)
    fun remove(position: Int)
    fun remove(iBaseItemVm: IBaseItemVm)
    fun clearAll()
}