package com.example.digitalacodemy.view.mappers

import com.example.digitalacodemy.model.dto.CourseDTO
import com.example.digitalacodemy.view.vo.CourseVO
import io.reactivex.Observable
import io.reactivex.functions.Function
import javax.inject.Inject

class CoursesMapper @Inject internal constructor() : Function<List<CourseDTO>, List<CourseVO>> {
    override fun apply(courseList: List<CourseDTO>): List<CourseVO> {
        return Observable.fromIterable(courseList)
            .map { courseDTO ->
                CourseVO(courseDTO.id, courseDTO.name, courseDTO.type, courseDTO.progress)
            }
            .toList()
            .blockingGet()
    }
}