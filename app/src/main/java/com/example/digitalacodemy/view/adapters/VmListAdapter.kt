package com.example.digitalacodemy.view.adapters

import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup

open class VmListAdapter : RecyclerView.Adapter<VmViewHolder>(), IBaseListAdapter<IBaseItemVm> {

    var mItems = ArrayList<IBaseItemVm>()

    override fun getItemCount() = mItems.size
    override fun getItemViewType(position: Int) = mItems[position].getLayoutId()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VmViewHolder {

        val inflater = LayoutInflater.from(parent.context)
        val viewDataBinding = DataBindingUtil
            .inflate<ViewDataBinding>(inflater!!, viewType, parent, false)

        return VmViewHolder(viewDataBinding)
    }

    override fun onBindViewHolder(holder: VmViewHolder, position: Int) {

        holder.binding.setVariable(mItems[position].brVariableId, mItems[position])
        holder.binding.executePendingBindings()
    }

    override fun add(newItem: IBaseItemVm) {

        mItems.add(newItem)
        notifyItemInserted(mItems.lastIndex)
    }

    override fun add(newItems: List<IBaseItemVm>) {
        val oldSize = mItems.size
        mItems.addAll(newItems)
        notifyItemRangeInserted(oldSize, newItems.size)
    }

    override fun add(position: Int, newItems: List<IBaseItemVm>) {
        val oldSize = mItems.size
        mItems.addAll(0, newItems)
        notifyItemRangeInserted(oldSize, newItems.size)
    }

    override fun clearAll() {
        mItems.clear()
        notifyDataSetChanged()
    }

    override fun getItemId(position: Int): Long {

        val pos = mItems.size - position
        return super.getItemId(pos)
    }

    override fun replaceItems(newItems: List<IBaseItemVm>) {
        mItems = ArrayList(newItems)
        notifyDataSetChanged()
    }

    override fun addAtPosition(pos: Int, newItem: IBaseItemVm) {

        mItems.add(pos, newItem)
        notifyItemInserted(pos)
    }

    override fun remove(position: Int) {

        mItems.removeAt(position)
        notifyItemRemoved(position)
    }

    override fun remove(iBaseItemVm: IBaseItemVm) {
        mItems.remove(iBaseItemVm)
        notifyDataSetChanged()
    }
}