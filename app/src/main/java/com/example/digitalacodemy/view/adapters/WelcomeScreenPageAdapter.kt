package com.example.digitalacodemy.view.adapters
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.example.digitalacodemy.R
import com.example.digitalacodemy.view.vo.WelcomeScreenVO
import kotlin.collections.ArrayList

class WelcomeScreenPageAdapter(var welcomeScreenList: ArrayList<WelcomeScreenVO> ) :
    PagerAdapter() {

    fun addScreen(welcomeScreen: WelcomeScreenVO, position: Int) {
        welcomeScreenList.add(position, welcomeScreen)
        notifyDataSetChanged()
    }


    override fun getCount(): Int {
        return welcomeScreenList.size
    }

    override fun isViewFromObject(@NonNull view: View, @NonNull `object`: Any): Boolean {
        return view === `object`
    }

    @NonNull
    override fun instantiateItem(@NonNull container: ViewGroup, position: Int): Any {
        val view = LayoutInflater.from(container.context)
            .inflate(R.layout.model_welcome_screen, container, false)
        view.tag = position
        container.addView(view)
        bind(welcomeScreenList[position], view)

        return view
    }

    fun getScreen(position: Int): WelcomeScreenVO {
        return welcomeScreenList[position]
    }

    private fun bind(item: WelcomeScreenVO, view: View) {

        val tvDescription = view.findViewById<TextView>(R.id.tvDescription)
        val imgScreen = view.findViewById<ImageView>(R.id.imgScreen)

        tvDescription.text = item.description

        Glide.with(imgScreen.context)
            .load(item.imgScreen)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any, target: com.bumptech.glide.request.target.Target<Drawable>,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable, model: Any,
                    target: com.bumptech.glide.request.target.Target<Drawable>,
                    dataSource: DataSource, isFirstResource: Boolean
                ): Boolean {
                    return if (isFirstResource) {
                        imgScreen.setImageDrawable(resource)
                        imgScreen.visibility = View.VISIBLE
                        true
                    } else
                        false
                }
            }).into(imgScreen)
    }


    fun removeScreen(location: Int) {
        welcomeScreenList.removeAt(location)
        notifyDataSetChanged()
    }

    override fun destroyItem(@NonNull container: ViewGroup, position: Int, @NonNull `object`: Any) {
        container.removeView(`object` as View)
    }


    override fun getItemPosition(@NonNull `object`: Any): Int {
        return POSITION_NONE
    }

}