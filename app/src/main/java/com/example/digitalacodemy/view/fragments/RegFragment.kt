package com.example.digitalacodemy.view.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import com.example.digitalacodemy.R
import com.example.digitalacodemy.databinding.FragmentRegBinding
import com.example.digitalacodemy.view_model.RegViewModel


class RegFragment : BaseFragment(), AdapterView.OnItemSelectedListener {

    lateinit var binding: FragmentRegBinding
    val viewModel: RegViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_reg,
            container, false)
        binding.viewModel = viewModel



        context?.let {
            ArrayAdapter.createFromResource(
                it, R.array.roles, android.R.layout.simple_spinner_item
            ).also { adapter ->
                // Specify the layout to use when the list of choices appears
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                // Apply the adapter to the spinner
                binding.spinner.adapter = adapter
            }
        }

        viewModel.result.observe(viewLifecycleOwner, {
            //if (it) Navigation.navi
            getFragmentNavController(R.id.nav_host_fragment)
                ?.navigate(RegFragmentDirections.actionRegFragmentToCursesFragment())
        })

        binding.spinner.onItemSelectedListener = this

        binding.btnNext.setOnClickListener { viewModel.postData() }



        binding.executePendingBindings()

        return binding.root
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, pos: Int, id: Long) {

        Log.d("myLog", "selected = ${parent?.getItemAtPosition(pos)}")
        viewModel.role = parent?.getItemAtPosition(pos).toString()
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }


}