package com.example.digitalacodemy.view.vo

import androidx.lifecycle.MutableLiveData
import com.example.digitalacodemy.R
import com.example.digitalacodemy.BR
import com.example.digitalacodemy.view.adapters.IBaseItemVm

class CourseVO (override val id: String?,
                var name: String,
                var type: String,
                var progress: String
) : IBaseItemVm {

    var clickElement = MutableLiveData<CourseVO>()

    override val brVariableId: Int
        get() = when (type) {
            "user" -> BR.vmCourseVOUser
            "general" -> BR.vmCourseVOGeneral
            "passed" -> BR.vmCourseVOPassed
            else -> BR.vmCourseVOGeneral
        }


    override fun getLayoutId(): Int {

        return when (type) {
            "user" -> R.layout.model_course_user
            "general" -> R.layout.model_course_general
            "passed" -> R.layout.model_course_passed
            else -> R.layout.model_course_general
        }


    }

}