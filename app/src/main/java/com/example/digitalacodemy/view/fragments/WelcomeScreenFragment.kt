package com.example.digitalacodemy.view.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.IdRes
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.example.digitalacodemy.R
import com.example.digitalacodemy.databinding.FragmentWelcomeScreenBinding
import com.example.digitalacodemy.view.adapters.WelcomeScreenPageAdapter
import com.example.digitalacodemy.view.vo.WelcomeScreenVO
import com.example.digitalacodemy.view_model.WelcomeScreenViewModel
import com.google.android.material.snackbar.Snackbar

class WelcomeScreenFragment : BaseFragment() {

    lateinit var binding: FragmentWelcomeScreenBinding

    private val viewModel: WelcomeScreenViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_welcome_screen,
            container, false)
        binding.viewModel = viewModel


        val welcomeScreenList = ArrayList<WelcomeScreenVO>()


        welcomeScreenList.add(WelcomeScreenVO(
            getString(R.string.w_screen_1),
            ResourcesCompat.getDrawable(resources, R.drawable.ic_launcher_foreground, null)))
        welcomeScreenList.add(WelcomeScreenVO(
            getString(R.string.v_screen_2),
            ResourcesCompat.getDrawable(resources, R.drawable.ic_launcher_foreground, null)))
        welcomeScreenList.add(WelcomeScreenVO(
            getString(R.string.v_screen_3),
            ResourcesCompat.getDrawable(resources, R.drawable.ic_launcher_foreground, null)))


        val welcomeScreenPageAdapter = WelcomeScreenPageAdapter(welcomeScreenList)

        binding.vpScreens.adapter = welcomeScreenPageAdapter

        binding.indicator.setViewPager(binding.vpScreens)

        viewModel.result.observe(viewLifecycleOwner, {
                result ->
            if (result) getFragmentNavController(R.id.nav_host_fragment)
                ?.navigate(WelcomeScreenFragmentDirections.actionWelcomeScreenFragmentToRegFragment())
        })



        binding.btnNext.setOnClickListener {
            val nextPage = binding.vpScreens.currentItem + 1
            if (nextPage == 3) viewModel.finish()
            else binding.vpScreens.currentItem = nextPage
        }

        binding.tvSkip.setOnClickListener { viewModel.finish() }


        binding.executePendingBindings()

        return binding.root
    }



}