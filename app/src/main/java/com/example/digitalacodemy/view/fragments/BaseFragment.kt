package com.example.digitalacodemy.view.fragments

import android.os.Bundle
import android.view.View
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import com.example.digitalacodemy.view_model.SharedViewModel
import com.google.android.material.snackbar.Snackbar

abstract class BaseFragment : Fragment() {

    protected val sharedViewModel: SharedViewModel by activityViewModels()


    override fun onPause() {
        super.onPause()
        //sharedViewModel.backPressed.removeObservers(this)
    }


    fun showError(errorMessage: String) {

        val view: View? = activity?.findViewById(android.R.id.content)

        view?.let { Snackbar.make(it, errorMessage, Snackbar.LENGTH_LONG).show() }
    }


    fun Fragment.getFragmentNavController(@IdRes id: Int) = activity?.let {
        return@let Navigation.findNavController(it, id)
    }

    fun navigateOnClickListener(resId: Int, args: Bundle) = Navigation
        .createNavigateOnClickListener(resId, args)
    fun navigateOnClickListener(resId: Int) = Navigation
        .createNavigateOnClickListener(resId)
}