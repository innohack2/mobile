package com.example.digitalacodemy.view.adapters

interface IBaseListItem {
    fun getLayoutId(): Int
}