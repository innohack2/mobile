package com.example.digitalacodemy.view.vo

data class UserVO (var progress: String, var role: String, var cookies: String)