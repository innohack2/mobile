package com.example.digitalacodemy.view.vo.separators

import com.example.digitalacodemy.R
import com.example.digitalacodemy.view.adapters.IBaseItemVm
import com.example.digitalacodemy.BR

class PassedCoursesSeparatorVO(override val id: String = "") : IBaseItemVm {
    override val brVariableId: Int
        get() = BR.vmPassedCoursesSeparatorVO
    override fun getLayoutId(): Int = R.layout.model_passed_courses_separator
}